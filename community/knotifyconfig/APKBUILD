# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=knotifyconfig
pkgver=5.102.0
pkgrel=0
pkgdesc="Configuration system for KNotify"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-only"
depends_dev="
	kcompletion-dev
	kconfig-dev
	ki18n-dev
	kio-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/knotifyconfig-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9f46710a283aa27fec22a70f0f7f9a3f756f1f5ae323447eca57b6c8c8cfe47459cd3eae998f3c79dc72ef633eeacc31d98fb4ebbd8b9099cc237141481389b8  knotifyconfig-5.102.0.tar.xz
"
