# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: knuxify <knuxify@gmail.com>
pkgname=kvantum
pkgver=1.0.8
pkgrel=0
pkgdesc="SVG-based theme engine for Qt5"
options="!check" # No testsuite
url="https://github.com/tsujan/Kvantum"
arch="all !armhf" # Limited by kwindowsystem-dev
license="GPL-2.0-or-later"
depends="hicolor-icon-theme"
makedepends="
	cmake
	kwindowsystem-dev
	libx11-dev
	libxext-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	qt5-qtx11extras-dev
	qt6-qtbase-dev
	qt6-qtsvg-dev
	qt6-qttools-dev
	samurai
	"
subpackages="$pkgname-lang $pkgname-qt5:_qt5 $pkgname-qt6:_qt6 $pkgname-themes::noarch"
install="$pkgname.post-install $pkgname.post-upgrade"
source="$pkgname-$pkgver.tar.gz::https://github.com/tsujan/Kvantum/archive/V$pkgver/kvantum-$pkgver.tar.gz"
builddir="$srcdir/Kvantum-$pkgver/Kvantum"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build-qt5 -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DENABLE_QT4=OFF \
		$CMAKE_CROSSOPTS
	cmake --build build-qt5

	cmake -B build-qt6 -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DENABLE_QT5=OFF \
		$CMAKE_CROSSOPTS
	cmake --build build-qt6
}

package() {
	DESTDIR="$pkgdir" cmake --install build-qt5
	DESTDIR="$pkgdir" cmake --install build-qt6
	rm -rf "$pkgdir"/usr/share/kde4
}

lang() {
	pkgdesc="Languages for package $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel lang"

	# We can't use default_lang since there's no /usr/share/locale, only
	# these two directories:
	amove usr/share/kvantumpreview/translations
	amove usr/share/kvantummanager/translations
}

_qt5() {
	pkgdesc="$pkgdesc (Qt5 support)"
	install_if="$pkgname=$pkgver-r$pkgrel qt5-qtbase"
	depends="kvantum"

	amove usr/lib/qt5
}

_qt6() {
	pkgdesc="$pkgdesc (Qt6 support)"
	install_if="$pkgname=$pkgver-r$pkgrel qt6-qtbase"
	depends="kvantum"

	amove usr/lib/qt6
}

themes() {
	pkgdesc="$pkgdesc (Pre-installed themes)"
	depends="kvantum"

	amove usr/share/themes
	amove usr/share/color-schemes
	amove usr/share/Kvantum
}

sha512sums="
b6689cbeed41fe43f00c5e8b563e3c405bf40436079f9ade6cc839d7339a1fda68ee7d29c80f51dd9db87ee2f6165e978c44e07fa3f944c0aec83a149c9221df  kvantum-1.0.8.tar.gz
"
