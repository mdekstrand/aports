# Contributor: nibon7 <nibon7@163.com>
# Maintainer: nibon7 <nibon7@163.com>
pkgname=v2ray
pkgver=5.2.1
pkgrel=1
pkgdesc="A platform for building proxies to bypass network restrictions"
url="https://v2fly.org"
arch="all"
license="MIT"
options="chmod-clean"
makedepends="go"
subpackages="$pkgname-openrc"
_geosite_ver=20230114135558
_geoip_ver=202301120046
source="$pkgname-core-$pkgver.tar.gz::https://github.com/v2fly/v2ray-core/archive/v$pkgver.tar.gz
	geosite-$_geosite_ver.dat::https://github.com/v2fly/domain-list-community/releases/download/$_geosite_ver/dlc.dat
	geoip-$_geoip_ver.dat::https://github.com/v2fly/geoip/releases/download/$_geoip_ver/geoip.dat
	geoip-only-cn-private-$_geoip_ver.dat::https://github.com/v2fly/geoip/releases/download/$_geoip_ver/geoip-only-cn-private.dat
	go-1.20-quic.patch
	v2ray.initd
	"

builddir="$srcdir/$pkgname-core-$pkgver"

case "$CARCH" in
ppc64le)
	# Failed accept TCP connection: accept tcp 127.0.0.1:35609: use of closed network connection
	# flaky tests
	options="$options !check"
	;;
esac

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local ldflags="
		-X github.com/v2fly/v2ray-core/v5.codename=$pkgname
		-X github.com/v2fly/v2ray-core/v5.version=$pkgver
		-X github.com/v2fly/v2ray-core/v5.build=$SOURCE_DATE_EPOCH
		-buildid=
		"
	go build -trimpath -ldflags "$ldflags" -o v2ray ./main
}

check() {
	# v5/infra/conf/geodata/memconservative failed on aarch64 and ppc64le
	# v5/infra/conf/{rule,synthetic/dns} failed on x86
	# v5/transport/internet/quic failed on x86_64
	local pkgs=$(go list ./... | grep -v \
		-e 'v5/infra/conf/geodata/memconservative$' \
		-e 'v5/infra/conf/rule$' \
		-e 'v5/infra/conf/synthetic/dns$' \
		-e 'v5/transport/internet/quic$' \
	)
	go test -v $pkgs
}

package() {
	install -Dm755 -d "$pkgdir"/etc/$pkgname
	install -m644 release/config/*.json -t "$pkgdir"/etc/$pkgname

	install -Dm755 -d "$pkgdir"/usr/share/$pkgname
	install -m644 "$srcdir"/geosite-$_geosite_ver.dat \
		"$pkgdir"/usr/share/$pkgname/geosite.dat
	install -m644 "$srcdir"/geoip-$_geoip_ver.dat \
		"$pkgdir"/usr/share/$pkgname/geoip.dat
	install -m644 "$srcdir"/geoip-only-cn-private-$_geoip_ver.dat \
		"$pkgdir"/usr/share/$pkgname/geoip-only-cn-private.dat

	install -Dm755 v2ray "$pkgdir"/usr/bin/v2ray

	install -Dm755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
}

sha512sums="
3c2603e2a0613ccb25b73ba32881c256bc9e74bd0e820f2adcc54ee6c832d30f404261a45b6c231ef6654bdb429a0ad1fb7ab3c538bd09c2a9a23f9d4f16ef3c  v2ray-core-5.2.1.tar.gz
622ff6aec60412a2bdfde49538093b561298d680212d496c772b44ef56bd7a05c6204c0140c2b8f436ca7e4c8da87bf3145033e3720ca7ac798b682b2d524d2f  geosite-20230114135558.dat
9a16645e263fccf4e4013b1cca7616e9865701ae201fe68bb23f2332356d63faf841003d3db4b445ef36f9996f4672a9bab8ad7224ccf0aca054b7c01e93b555  geoip-202301120046.dat
66639e2069fc51051446af024856625581f0448e2ca20418b43b50159a8a5ea6092e40548af2b034182af22af9854a405789937b334863983e6dd673258d5f12  geoip-only-cn-private-202301120046.dat
4febfbc36037dee5d0c56d4c550ee6345657d9d94b67339b1f71684498d88628f143e5a191de401d40f63528e0f492968aebd518d2da281506c917f0b31f69a6  go-1.20-quic.patch
5b586b2a927da3a96c279bed3330bb9ee397b3e880e573ae7dc108d35864576cf72e5ceacb74b497829d514db10bb7f6db85c3e5383005288ab62cd61510ebaa  v2ray.initd
"
