# Contributor: Clayton Craft <clayton@craftyguy.net>
# Maintainer: Clayton Craft <clayton@craftyguy.net>
pkgname=nemo
pkgver=5.6.2
pkgrel=0
pkgdesc="File manager for the Cinnamon desktop environment"
# s390x and ppc64le blocked by exempi
# riscv64 disabled due to missing rust in recursive dependency
arch="all !s390x !ppc64le !riscv64"  # exempi, libexif-dev not available for all archs
url="https://github.com/linuxmint/nemo"
license="GPL-2.0-only"
makedepends="
	cinnamon-desktop-dev
	dconf-dev
	exempi-dev
	gobject-introspection-dev
	gvfs-dev
	intltool
	libexif-dev
	libgsf-dev
	libnotify-dev
	libxml2-dev
	meson
	python3
	xapp-dev
	"
checkdepends="xvfb-run"
source="https://github.com/linuxmint/nemo/archive/$pkgver/nemo-$pkgver.tar.gz"
subpackages="$pkgname-doc $pkgname-dev"
options="!check" # tests are broken: https://github.com/linuxmint/nemo/issues/2501

build() {
	abuild-meson . output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

check() {
	xvfb-run meson test -C output
}
sha512sums="
e3606447fc7b20ad310ccc110c430959b05f237c0d08df8ddc86c654a3019f796c05dc1494522b4eb0af9bf17a302d8fe20d25e92788dc3013f80dc6bfc739f1  nemo-5.6.2.tar.gz
"
