# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwindowsystem
pkgver=5.102.0
pkgrel=0
pkgdesc="Access to the windowing system"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="MIT AND (LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="qt5-qtx11extras-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev doxygen libxrender-dev xcb-util-keysyms-dev xcb-util-wm-dev samurai"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwindowsystem-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# kwindowsystem-kwindowinfox11test hangs
	# kwindowsystem-kwindowsystemx11test, kwindowsystem-kwindowsystem_threadtest and kwindowsystem-netrootinfotestwm are broken
	# kwindowsystem-netwininfotestwm is broken on s390x
	local skipped_tests="kwindowsystem-("
	local tests="
		kwindowinfox11test
		kwindowsystemx11test
		kwindowsystem_threadtest
		netrootinfotestwm
		"
	case "$CARCH" in
		s390x) tests="$tests
			netwininfotestwm
			"
	esac
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)"
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
bdd2045c3ec8d78e912f9291759b6131dfd4bd944fb510b92045820b0e02dfceeb1e80efaf2cb28dda6150464be015581da6dfaf632c03d24cf8cdaf6112eaac  kwindowsystem-5.102.0.tar.xz
"
