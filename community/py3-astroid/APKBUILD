# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-astroid
pkgver=2.14.1
pkgrel=0
pkgdesc="A new abstract syntax tree from Python's ast"
url="https://pylint.pycqa.org/projects/astroid/en/latest/"
arch="noarch"
license="LGPL-2.1-or-later"
depends="python3 py3-lazy-object-proxy py3-wrapt"
replaces="py-logilab-astng"
makedepends="
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest py3-typing-extensions"
source="py3-astroid-$pkgver.tar.gz::https://github.com/PyCQA/astroid/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/astroid-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/astroid-*.whl
}

sha512sums="
47dd132e66645c3da639c68011f4691409af38affa80c5b1863706a864fb8458cdfd100f86929fdaac027a514d285132d8865d2d40996c583077384696a84123  py3-astroid-2.14.1.tar.gz
"
