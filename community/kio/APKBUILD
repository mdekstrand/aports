# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kio
pkgver=5.102.0
pkgrel=0
pkgdesc="Resource and network access abstraction"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends_dev="
	karchive-dev
	kbookmarks-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kitemviews-dev
	kjobwidgets-dev
	knotifications-dev
	kservice-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	qt5-qtscript-dev
	solid-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	kdoctools-dev
	libxml2-dev
	libxslt-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kio-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-kwallet"
options="!check" # Fails due to requiring physical devices not normally available and test 14 hangs

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

kwallet() {
	pkgdesc="$pkgname KWallet integration"

	amove usr/lib/qt5/plugins/kf5/kiod/kpasswdserver.so
}

sha512sums="
29f46ace3a902a24c3a266413b291eac43bcfc5da76dfbc927f3d0e36c3902f0abf195b0cd97fd163a62b85a3d0bbb006dc73bc0fc43a88e47f72e7991536dc4  kio-5.102.0.tar.xz
"
