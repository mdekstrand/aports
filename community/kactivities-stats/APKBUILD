# Maintainer: Bart Ribbers <bribbers@disroot.org>
# Contributor: Bart Ribbers <bribbers@disroot.org>
pkgname=kactivities-stats
pkgver=5.102.0
pkgrel=0
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
pkgdesc="A library for accessing the usage data collected by the activities system"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="boost-dev kconfig-dev kactivities-dev graphviz-dev qt5-qttools-dev qt5-qtdeclarative-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qtbase-dev samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kactivities-stats-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
9537499b1210ba69532346aafc8ca73bdd811840b816161a4e983a76f2e77f06c970489ea19d3a063c755fd3ee4f985fde30ea47623f1c364968c1bfcf644ffe  kactivities-stats-5.102.0.tar.xz
"
