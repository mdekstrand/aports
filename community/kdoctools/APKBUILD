# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdoctools
pkgver=5.102.0
pkgrel=0
pkgdesc="Documentation generation from docbook"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="
	docbook-xml
	docbook-xsl
	"
depends_dev="
	karchive-dev
	ki18n-dev
	libxml2-dev
	libxml2-utils
	libxslt-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	perl-uri
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdoctools-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
80271983a91887a3266d5ae5b66e25f55bbea202e6440cd72f666c49b003d07d00092d2bee3ed4218d181781559e7446375acae128f2dd8143250e0b7889d9a8  kdoctools-5.102.0.tar.xz
"
