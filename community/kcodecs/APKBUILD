# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcodecs
pkgver=5.102.0
pkgrel=0
pkgdesc="Provide a collection of methods to manipulate strings using various encodings"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
makedepends="
	doxygen
	extra-cmake-modules
	gperf
	graphviz
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcodecs-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(rfc2047|kcharsets)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6d6dc13ed7397a7b051b764e6a7e8fbf25279fcc9fe51fc3d91080c4744d3fc450b9ae331332d31ba4d26f3e5390c00a4d695f83aecc83339a053758f8658205  kcodecs-5.102.0.tar.xz
"
