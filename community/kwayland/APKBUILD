# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwayland
pkgver=5.102.0
pkgrel=0
pkgdesc="Qt-style Client and Server library wrapper for the Wayland libraries"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://www.kde.org"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	qt5-qtwayland-dev
	wayland-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	plasma-wayland-protocols
	qt5-qtbase-dev
	qt5-qttools-dev
	wayland-protocols
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwayland-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-dbg"
options="!check" # Fails due to requiring running Wayland compositor

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
fbda5683268f9a242b76f02fd86fffc7414c054ca986ae9ea427549c4024e6cc2946f92344e0a34450787c6d7d3ab0169f8cd1649fcb86ece0579f9a6959f333  kwayland-5.102.0.tar.xz
"
