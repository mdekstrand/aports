# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer:
pkgname=bear
pkgver=3.0.21
pkgrel=5
pkgdesc="Tool which generates a compilation database for clang tooling"
url="https://github.com/rizsotto/Bear"
arch="all"
license="GPL-3.0-or-later"
makedepends="
	c-ares-dev
	cmake
	fmt-dev
	grpc-dev
	gtest-dev
	nlohmann-json
	protobuf-dev
	re2-dev
	samurai
	spdlog-dev
	sqlite-dev
	"
checkdepends="llvm-test-utils"
subpackages="$pkgname-doc"
source="https://github.com/rizsotto/Bear/archive/$pkgver/bear-$pkgver.tar.gz"
builddir="$srcdir/Bear-$pkgver"

# XXX: Tests fail when ccache is enabled.

# armv7, armhf and aarch64 have some failing tests.
case "$CARCH" in
	armhf|armv7|aarch64) options="!check" ;;
esac

prepare() {
	default_prepare
	case "$CARCH" in
		x86) rm -f test/cases/intercept/preload/signal_outside_build.sh ;;
	esac
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_INSTALL_LIBEXECDIR=libexec/bear \
		-DCMAKE_BUILD_TYPE=MinSizeRel
	cmake --build build
}

check() {
	cd build
	ctest --verbose --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9fe43a52fb30b7b413244576c6ea0d493bb720e54d0b6263280224efef4b0432dee46697df160ba7e76bc1a5c8735fe814e0eb686ed797658b563bcb8c5bb7e5  bear-3.0.21.tar.gz
"
