# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kholidays
pkgver=5.102.0
pkgrel=0
pkgdesc="Support for icon themes"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later"
depends_dev="
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kholidays-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d285c9b14f9be8bf74a3a40792e250965e0338dd81ce24408fa9c81715c5cc5de19dbc7243f8b545a8d17cdffd175bcdacc38b641060e720c34d147124e2ef54  kholidays-5.102.0.tar.xz
"
