# Maintainer: psykose <alice@ayaya.dev>
pkgname=lua-language-server
pkgver=3.6.10
pkgrel=0
pkgdesc="Language Server for Lua"
url="https://github.com/LuaLS/lua-language-server"
arch="all !s390x !ppc64le" # ftbfs
license="MIT"
makedepends="bash samurai"
source="https://github.com/LuaLS/lua-language-server/archive/refs/tags/$pkgver/lua-language-server-$pkgver.tar.gz
	lua-language-server-submodules-$pkgver.zip.noauto::https://github.com/LuaLS/lua-language-server/releases/download/$pkgver/lua-language-server-$pkgver-submodules.zip
	wrapper
	"
options="!check" # no tests

prepare() {
	unzip -o "$srcdir"/lua-language-server-submodules-$pkgver.zip.noauto \
		-d "$builddir"
	default_prepare
}

build() {
	ninja -C 3rd/luamake -f compile/ninja/linux.ninja
	./3rd/luamake/luamake rebuild
}

package() {
	install -Dm755 "$srcdir"/wrapper "$pkgdir"/usr/bin/lua-language-server
	install -Dm755 bin/lua-language-server \
		-t "$pkgdir"/usr/lib/lua-language-server/bin
	install -Dm644 bin/main.lua \
		-t "$pkgdir"/usr/lib/lua-language-server/bin
	install -Dm644 debugger.lua main.lua \
		-t "$pkgdir"/usr/lib/lua-language-server
	cp -a locale meta script "$pkgdir"/usr/lib/lua-language-server
}

sha512sums="
787db7482ba46b98521a3bdacfec35992f2a9628b91d43191a6824281920d29892c6286c5631122c9f45a47d64e113b452aaea13c5b147a7d6d5b360d35ec463  lua-language-server-3.6.10.tar.gz
a49c28990e00115eaac83e615079192adfd39655436babc86dafe3660912c10346ab99c418272ac4a9c2113a98921b9b07c0c20bdf62fe633872417127247509  lua-language-server-submodules-3.6.10.zip.noauto
75a65e2e084b1f8e11b88f874ad399f51dbd280c02eaa0d8aa79e7c1fdc9e734104ef4f418f733b8d4df5eadfee8683087cc3d13e783e6104c4e7ffa4671cdf3  wrapper
"
