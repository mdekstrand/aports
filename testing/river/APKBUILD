# Contributor: Anjandev Momi <anjan@momi.ca>
# Maintainer: Anjandev Momi <anjan@momi.ca>
pkgname=river
pkgver=0.2.3
pkgrel=0
pkgdesc="Dynamic Tiling Wayland Compositor"
url="https://github.com/riverwm/river"
arch="x86_64 aarch64" # limited by zig aport
license="GPL-3.0-only"
makedepends="
	libevdev-dev
	libxkbcommon-dev
	pixman-dev
	scdoc
	wayland-dev
	wayland-protocols
	wlroots-dev
	zig
	"
depends="seatd"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/riverwm/river/releases/download/v$pkgver/river-$pkgver.tar.gz"

# We may want other than "baseline" for other targets, when enabled by zig
case "$CARCH" in
	aarch64|x86_64) cputarget=baseline ;;
esac

build() {
	# This installs it to $builddir/out
	DESTDIR="$builddir/out" zig build -Drelease-safe -Dxwayland --prefix /usr install \
		${cputarget:+-Dcpu="$cputarget"}
}

check() {
	zig build test
}

package() {
	mkdir -p "$pkgdir"
	cp -r out/* "$pkgdir"

	# Fix location of pkgconfig files, must be fixed upstream
	mkdir -p "$pkgdir"/usr/lib
	mv "$pkgdir"/usr/share/pkgconfig "$pkgdir"/usr/lib

	# Install example configuration
	install -Dm0644 example/init -t "$pkgdir"/usr/share/doc/river/examples

	# Fix location of fish completion
	mkdir -p "$pkgdir"/usr/share/fish/completions/
	mv "$pkgdir"/usr/share/fish/vendor_completions.d/*.fish \
		"$pkgdir"/usr/share/fish/completions/
	rm -rf "$pkgdir"/usr/share/fish/vendor_completions.d
}

sha512sums="
7cd8410d19a32c8af5a1b91080b62d11ba6fc50d18ebd9f3ddb53c753d45ffa43b0256d713fbf4511cd08db9d360e0269bc61407b44f04539ecb6cc37f617fcf  river-0.2.3.tar.gz
"
