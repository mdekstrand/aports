# Maintainer: Hugo Osvaldo Barrera <hugo@whynothugo.nl>
pkgname=ruff
pkgver=0.0.244
pkgrel=0
pkgdesc="Extremely fast Python linter"
url="https://github.com/charliermarsh/ruff"
# ppc64le, s390x, riscv64: ring
# x86, armhf, armv7: fails tests on 32-bit
arch="all !x86 !armhf !armv7 !ppc64le !s390x !riscv64"
license="MIT"
makedepends="maturin cargo py3-installer"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/charliermarsh/ruff/archive/v$pkgver/ruff-$pkgver.tar.gz"

export CARGO_PROFILE_RELEASE_OPT_LEVEL=2

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	maturin build --release --frozen --manylinux off

	./target/release/ruff --generate-shell-completion bash > $pkgname.bash
	./target/release/ruff --generate-shell-completion fish > $pkgname.fish
	./target/release/ruff --generate-shell-completion zsh > $pkgname.zsh
}

check() {
	cargo test --frozen
}

package() {
	python3 -m installer -d "$pkgdir" \
		target/wheels/*.whl

	install -Dm644 $pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
0140d2aee9d75eeb9a5bebab82be307c8854c7f16ca2b5362bd8e5bff2f68fd08e22b629a6acc516de24337381f8fd62943c166643348c8967158a66d66bc732  ruff-0.0.244.tar.gz
"
